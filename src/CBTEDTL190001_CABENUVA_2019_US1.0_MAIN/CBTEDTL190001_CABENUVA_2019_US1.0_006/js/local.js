// --- local.js --- //

com.gsk.mt.onInit = function() {
	
};


$(document).ready(function() {

    const tab_1_isi = $('#tab-1-isi');
    const tab_2_isi = $('#tab-2-isi');

    $('tspan').find('[font-family*="AvantGarde-Demi"]').each(function(){
       // $(this).attr('font-family').replace(/AvantGarde-Demi/g, 'ITC-ITCAvantGardePro-Demi');
        //$(this).attr('font-family').replace(/AvantGarde-Demi/g, 'ITC-ITCAvantGardePro-Demi');
       $(this).replace('AvantGarde-Demi','ITC-ITCAvantGardePro-Demi');

        //this.font-family.replace('AvantGarde-Demi','ITC-ITCAvantGardePro-Demi');
    }); 

    $("#tab_1_button").on("click",()=>{

        handleTab1();
        tab_1_isi.show();
        tab_2_isi.hide();
    })
    
   
    $("#tab_2_button").on("click", ()=>{

        handleTab2();
        tab_2_isi.show();
        tab_1_isi.hide();
    });
    

 

    // $('tspan').each(function() {

    //    var svgFont =  $(this).attr('font-family');

    //    //console.log(svgFont.typeOf);

    //    console.log(typeof svgFont);

    // //    for (var i = 0; i <= svgFont.length; i++) { 
    // //        if((svgFont[i]) == ('AvantGarde-Demi')) {
    // //         console.log('true');
    // //        }

    //    }
      

      // console.log(svgFont);  //logs fonts correctly from tspans

      

    //    switch (svgFont) {
    //     case 'AvantGarde-Demi':

    //     console.log('true');
    //            // $(this).attr("font-family", "ITC-ITCAvantGardePro-Demi");
    //           //  $(this).attr('font-family') = 'ITC-ITCAvantGardePro-Demi';
    //         // $(this).attr('font-family').replace(/AvantGarde-Demi/g, 'ITC-ITCAvantGardePro-Demi');
    //        // $(this).text(text.replace(/ITCAvantGardePro-Bk/g, 'ITC-ITCAvantGardePro-Bk'));
    //       break;
      
    //   }

     
       
//
   




    $("#viral-efficacy-btn").on("click", function() {
        $("#viral-efficacy-dialog").removeClass("hidden"); 
    });

    $("#atlas-tab").on("click", function() {
        $("#flair-tab").removeClass("active"); 
        $("#atlas-tab").addClass("active");
        $(".svg-container.flair").removeClass("active");
        $(".svg-container.atlas").addClass("active");
        $(".modal-list.flair").removeClass("active");
        $(".modal-footnotes.flair").removeClass("active");
        $(".modal-list.atlas").addClass("active");
        $(".modal-footnotes.atlas").addClass("active");
    });

    $("#flair-tab").on("click", function() {
        $("#atlas-tab").removeClass("active"); 
        $("#flair-tab").addClass("active");
        $(".svg-container.atlas").removeClass("active");
        $(".svg-container.flair").addClass("active");
        $(".modal-list.atlas").removeClass("active");
        $(".modal-footnotes.atlas").removeClass("active");
        $(".modal-list.flair").addClass("active");
        $(".modal-footnotes.flair").addClass("active");
    });

    setSelectedSlide()
});

const setSelectedSlide = ()=>{
    const sectionName = sessionStorage.getItem('slideSection');
    console.log(sectionName);
    //let section;
    let section;
    switch (sectionName) {
        case 'outcomes-cvf':
            handleTab2();
            break;
        // case '':
        //     section = 1;
        //     break;
    }

    sessionStorage.removeItem('slideSection');
}